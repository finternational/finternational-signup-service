FROM mcr.microsoft.com/dotnet/sdk:6.0 AS builder
WORKDIR /src
EXPOSE 80
COPY . .

#Restore Dependencies
RUN dotnet restore "finternational-signup-service/finternational-signup-service.csproj" -s "https://api.nuget.org/v3/index.json" -s "https://gitlab.com/api/v4/projects/35958452/packages/nuget/index.json"
RUN dotnet restore "finternational-signup-service-tests/finternational-signup-service-tests.csproj" -s "https://api.nuget.org/v3/index.json" -s "https://gitlab.com/api/v4/projects/32083342/packages/nuget/index.json"

#RUN TESTS
RUN dotnet test "finternational-signup-service-tests/finternational-signup-service-tests.csproj" /p:CollectCoverage=true

#Publish App
RUN dotnet publish "finternational-signup-service/finternational-signup-service.csproj" -c Release -o /app/publish
FROM mcr.microsoft.com/dotnet/sdk:6.0  AS final
#Copy App and Run
WORKDIR /app
ENV ASPNETCORE_URLS=http://+:80
COPY --from=builder /app/publish /app
ENTRYPOINT ["dotnet", "finternational-signup-service.dll"]
