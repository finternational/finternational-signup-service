﻿namespace finternational_signup_service.Security
{
    public interface IAuthorisationProvider
    {
        string GetAuthorisationEncoded();
    }
}
