﻿using finternational_signup_service.Configuration;
using System.Text;

namespace finternational_signup_service.Security
{
    public class AuthorisationProvider : IAuthorisationProvider
    {
        public AuthorisationSettings authorisationSettings;
        public AuthorisationProvider(AuthorisationSettings authorisationSettings)
        {
            this.authorisationSettings = authorisationSettings;
        }

        public string GetAuthorisationEncoded()
        {
            var plainTextBytes = Encoding.UTF8.GetBytes($"{authorisationSettings.Username}:{authorisationSettings.Password}");

            return $"Basic {Convert.ToBase64String(plainTextBytes)}";
        }
    }
}
