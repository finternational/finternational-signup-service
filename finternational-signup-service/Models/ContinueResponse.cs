﻿namespace finternational_signup_service.Models
{
    public class ContinueResponse
    {
        public string Version = "1.0.0";
        public string Action = "Continue";
    }
}
