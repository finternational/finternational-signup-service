﻿using System.Text.Json.Serialization;

namespace finternational_signup_service.Models
{
    public class ValidateSignUpRequest
    {
        public string? GivenName { get; set; }
        public string? Surname { get; set; }

        [JsonPropertyName("extension_ad21a69570ca45b48611826786ce20a4_TeamName")]
        public string? TeamName { get; set; }
    }
}
