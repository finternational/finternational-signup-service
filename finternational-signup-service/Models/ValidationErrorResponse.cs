﻿namespace finternational_signup_service.Models
{
    public class ValidationErrorResponse
    {
         public ValidationErrorResponse(string errorMessage)
         {
             this.UserMessage = errorMessage;
         }

        public string Version { get; } = "1.0.0";
        public int Status { get; } = 400;
        public string Action { get;  } = "ValidationError";
        public string UserMessage { get; }
    }
}
