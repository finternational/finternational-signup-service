﻿using finternational_signup_service.Validation;

namespace finternational_signup_service.Configuration
{
    public static class ValidationConfigurationExtensions
    {
        public static void AddValidationLayer(this IServiceCollection services)
        {
            services.AddTransient<ISignUpRequestValidator, SignUpRequestValidator>();
        }
    }
}