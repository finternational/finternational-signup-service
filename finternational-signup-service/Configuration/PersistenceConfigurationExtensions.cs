﻿using finternational_common.Persistence;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories;
using finternational_common.Persistence.Repositories.Interfaces;

namespace finternational_signup_service.Configuration
{
    public static class PersistenceConfigurationExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services)
        {
            services.AddTransient<IConnectionProvider, ConnectionProvider>();
            services.AddTransient<ICommandExecutor, CommandExecutor>();
            services.AddTransient<IUnitOfWorkProvider, UnitOfWorkProvider>();
        }
    }
}
