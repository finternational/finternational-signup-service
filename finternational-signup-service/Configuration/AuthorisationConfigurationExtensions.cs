﻿using finternational_signup_service.Security;

namespace finternational_signup_service.Configuration
{
    public static class AuthorisationConfigurationExtensions
    {
        public static void AddSecurityLayer(this IServiceCollection services)
        {
            services.AddTransient<IAuthorisationProvider, AuthorisationProvider>();
        }
    }
}
