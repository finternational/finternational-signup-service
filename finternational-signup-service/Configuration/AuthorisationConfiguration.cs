﻿namespace finternational_signup_service.Configuration
{
    public record AuthorisationSettings(string Username, string Password) { }
    public static class AuthorisationConfiguration
    {
        public static void RegisterAuthorisationSettings(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(new AuthorisationSettings(
                config.GetValue<string>("Security:Username"), config.GetValue<string>("Security:Password")));
        }
    }
}
