using finternational_common.Persistence.Interfaces;
using finternational_signup_service.Models;
using finternational_signup_service.Security;
using finternational_signup_service.Validation;
using Microsoft.AspNetCore.Mvc;

namespace finternational_signup_service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SignUpController : ControllerBase
    {
        private readonly ContinueResponse continueResponse;

        private readonly IAuthorisationProvider authorisationProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly ISignUpRequestValidator signUpRequestValidator;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        public SignUpController(IAuthorisationProvider authorisationProvider, IConnectionProvider connectionProvider, 
            ISignUpRequestValidator signUpRequestValidator, PersistenceSettings persistenceSettings, IUnitOfWorkProvider unitOfWorkProvider)
        {
            this.authorisationProvider = authorisationProvider;
            this.signUpRequestValidator = signUpRequestValidator;
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;

            this.continueResponse = new ContinueResponse();
        }

        [HttpPost("validate")]
        public async Task<IActionResult> ValidateSignUp([FromBody]ValidateSignUpRequest request, [FromHeader]string Authorization)
        {
            if (Authorization == this.authorisationProvider.GetAuthorisationEncoded())
            {
                var errorMessage = this.signUpRequestValidator.Validate(request);

                if (errorMessage != string.Empty)
                {
                    return new BadRequestObjectResult(new ValidationErrorResponse(errorMessage));
                }

                return new OkObjectResult(continueResponse);
            }

            return new UnauthorizedResult();          
        }

        [HttpPost("")]
        public async Task<IActionResult> CompleteSignUp([FromBody] CompleteSignUpRequest request, [FromHeader] string Authorization)
        {
            if (Authorization == this.authorisationProvider.GetAuthorisationEncoded())
            {
                try
                {
                    using (var unitOfWork = this.unitOfWorkProvider.Create(this.persistenceSettings.ConnectionString))
                    {
                        //shouldn't ever be empty at this point as it's sent by the b2c connector
                        await unitOfWork.TeamRepository.Create(request.ObjectId, request.TeamName, request.GivenName, request.Surname);
                        await unitOfWork.EntryRepository.CreateEntries(request.ObjectId);

                        await unitOfWork.Save();
                    }
                    
                    return new OkObjectResult(continueResponse);
                }
                catch (Exception ex)
                {
                    return new BadRequestResult();
                    //at some point queue a message to retry this?
                }
            }

            return new UnauthorizedResult();
        }
    }
}