﻿using finternational_signup_service.Models;
using System.Text;

namespace finternational_signup_service.Validation
{
    public class SignUpRequestValidator : ISignUpRequestValidator
    {
        public string Validate(ValidateSignUpRequest request)
        {
            var errors = new List<string>();

            if (string.IsNullOrWhiteSpace(request.GivenName))
            {
                errors.Add("given name");
            }

            if (string.IsNullOrWhiteSpace(request.Surname))
            {
                errors.Add("surname");
            }

            if (string.IsNullOrWhiteSpace(request.TeamName))
            {
                errors.Add("team name");
            }

            if (errors.Count == 0)
            {
                return string.Empty;
            }


            StringBuilder sb = new StringBuilder("Please provide a valid ");
            if (errors.Count > 0)
            {
                sb.Append(errors[0]);

                if (errors.Count == 2)
                {
                    sb.Append($" and {errors[1]}");
                }

                if (errors.Count > 2)
                {
                    sb.Append($", {errors[1]} and {errors[2]}");
                }

                return sb.ToString();
            }

            return string.Empty;
        }
    }
}
