﻿using finternational_signup_service.Models;

namespace finternational_signup_service.Validation
{
    public interface ISignUpRequestValidator
    {
        string Validate(ValidateSignUpRequest request);
    }
}
