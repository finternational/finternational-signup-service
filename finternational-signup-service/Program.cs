using finternational_signup_service;
using finternational_signup_service.Configuration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.RegisterPersistenceSettings(builder.Configuration);
builder.Services.RegisterAuthorisationSettings(builder.Configuration);
builder.Services.AddSecurityLayer();
builder.Services.AddPersistenceLayer();
builder.Services.AddValidationLayer();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
