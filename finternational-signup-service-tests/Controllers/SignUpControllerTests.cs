﻿using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using finternational_signup_service;
using finternational_signup_service.Controllers;
using finternational_signup_service.Models;
using finternational_signup_service.Security;
using finternational_signup_service.Validation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;

using System.Threading.Tasks;
using Xunit;

namespace finternational_signup_service_tests.Controllers
{
    public class SignUpControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task ValidateSignUpReturns401IfAuthorisationHeaderIsIncorrect()
        {
            var settings = new PersistenceSettings("");
            this.autoMocker.Use(settings);

            this.autoMocker.GetMock<IAuthorisationProvider>()
                .Setup(x => x.GetAuthorisationEncoded())
                .Returns("unauth");

            var sut = this.autoMocker.CreateInstance<SignUpController>();

            var result = await sut.ValidateSignUp(new ValidateSignUpRequest(), "Bearer 1234") as UnauthorizedResult;

            Assert.Equal(401, result.StatusCode);
        }

        [Fact]
        public async Task ValidateSignUpReturnsBadRequestIfRequestIsInvalid()
        {
            var signupReq = new ValidateSignUpRequest();
            var errorMessage = "There's an error";
            var settings = new PersistenceSettings("");
            this.autoMocker.Use(settings);

            this.autoMocker.GetMock<IAuthorisationProvider>()
                .Setup(x => x.GetAuthorisationEncoded())
                .Returns("auth");

            this.autoMocker.GetMock<ISignUpRequestValidator>()
                .Setup(x => x.Validate(signupReq))
                .Returns(errorMessage);


            var sut = this.autoMocker.CreateInstance<SignUpController>();

            var result = await sut.ValidateSignUp(signupReq, "auth") as BadRequestObjectResult;
            var resultObject = result?.Value as ValidationErrorResponse;

            Assert.Equal(400, result?.StatusCode);
            Assert.Equal(errorMessage, resultObject?.UserMessage);
            Assert.Equal("1.0.0", resultObject?.Version);
            Assert.Equal(400, resultObject?.Status);
            Assert.Equal("ValidationError", resultObject?.Action);
        }

        [Fact]
        public async Task ValidateSignupRequestReturnsOkIfRequestIsValid()
        {
            var signupReq = new ValidateSignUpRequest();
            var settings = new PersistenceSettings("");
            this.autoMocker.Use(settings);

            this.autoMocker.GetMock<IAuthorisationProvider>()
                .Setup(x => x.GetAuthorisationEncoded())
                .Returns("auth");

            this.autoMocker.GetMock<ISignUpRequestValidator>()
                .Setup(x => x.Validate(signupReq))
                .Returns(string.Empty);

            var sut = this.autoMocker.CreateInstance<SignUpController>();

            var result = await sut.ValidateSignUp(signupReq, "auth") as OkObjectResult;
            var resultObject = result.Value as ContinueResponse;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal("1.0.0", resultObject.Version);
            Assert.Equal("Continue", resultObject.Action);

        }

        [Fact]
        public async Task CompleteSignUpReturns401IfAuthorisationHeaderIsIncorrect()
        {
            var settings = new PersistenceSettings("");
            this.autoMocker.Use(settings);

            this.autoMocker.GetMock<IAuthorisationProvider>()
                .Setup(x => x.GetAuthorisationEncoded())
                .Returns("unauth");

            var sut = this.autoMocker.CreateInstance<SignUpController>();

            var result = await sut.CompleteSignUp(
                new CompleteSignUpRequest() { ObjectId = "", GivenName = "", Surname = "", TeamName = "" }, 
                "Bearer 1234") as UnauthorizedResult;

            Assert.Equal(401, result.StatusCode);
        }

        [Fact]
        public async Task CompleteSignUpReturnsCorrectResult()
        {
            var settings = new PersistenceSettings("");
            this.autoMocker.Use(settings);

            this.autoMocker.GetMock<IAuthorisationProvider>()
                .Setup(x => x.GetAuthorisationEncoded())
                .Returns("Bearer 1234");

            var signupRequest = new CompleteSignUpRequest()
            {
                ObjectId = "id",
                GivenName = "name",
                Surname = "surname",
                TeamName = "teamname"
            };

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.TeamRepository.Create(signupRequest.ObjectId, signupRequest.TeamName, signupRequest.GivenName, signupRequest.Surname));
            mockUnitOfWork
                .Setup(x => x.EntryRepository.CreateEntries(signupRequest.ObjectId));

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.Create(""))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<SignUpController>();

            var result = await sut.CompleteSignUp(signupRequest, "Bearer 1234") as OkObjectResult;
            var resultObject = result.Value as ContinueResponse;

            this.autoMocker.GetMock<IUnitOfWork>()
                .Verify(x => x.TeamRepository.Create(signupRequest.ObjectId, signupRequest.TeamName, signupRequest.GivenName, signupRequest.Surname), Times.Once);
            this.autoMocker.GetMock<IUnitOfWork>()
                .Verify(x => x.EntryRepository.CreateEntries(signupRequest.ObjectId), Times.Once);
            this.autoMocker.GetMock<IUnitOfWork>()
                .Verify(x => x.Save(), Times.Once);
               
            Assert.Equal(200, result.StatusCode);
            Assert.Equal("1.0.0", resultObject.Version);
            Assert.Equal("Continue", resultObject.Action);
        }
    }
}
