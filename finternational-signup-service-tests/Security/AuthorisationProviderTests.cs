﻿using finternational_signup_service;
using finternational_signup_service.Configuration;
using finternational_signup_service.Security;
using Moq.AutoMock;
using Xunit;

namespace finternational_signup_service_tests.Security
{
    public class AuthorisationProviderTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void GetAuthorisationEncodedReturnsCorrectResult()
        {
            var stubSettings = new AuthorisationSettings("Username", "Password");
            this.autoMocker.Use(stubSettings);

            var sut = this.autoMocker.CreateInstance<AuthorisationProvider>();

            var result = sut.GetAuthorisationEncoded();

            Assert.Equal("Basic VXNlcm5hbWU6UGFzc3dvcmQ=", result);
        }
    }
}
