using finternational_signup_service.Models;
using finternational_signup_service.Validation;
using Moq.AutoMock;
using Xunit;

namespace finternational_signup_service_tests
{
    public class SignUpRequestValidatorTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Theory]
        [InlineData("", "", "", "Please provide a valid given name, surname and team name")]
        [InlineData("", "johnson", "", "Please provide a valid given name and team name")]
        [InlineData("John", "Johnson", "", "Please provide a valid team name")]
        [InlineData("John", "Johnson", "A team", "")]
        public void ValidateReturnsCorrectResults(string givenName, string surname, string teamName, string expectedResult)
        {
            var request = new ValidateSignUpRequest() { GivenName = givenName, Surname = surname, TeamName = teamName };

            var sut = this.autoMocker.CreateInstance<SignUpRequestValidator>();

            var result = sut.Validate(request);

            Assert.Equal(expectedResult == "" ? string.Empty : expectedResult, result);
        }
    }
}